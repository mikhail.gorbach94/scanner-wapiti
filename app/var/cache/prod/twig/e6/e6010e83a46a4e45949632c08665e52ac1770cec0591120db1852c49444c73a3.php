<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/pages/cookie.html.twig */
class __TwigTemplate_46d207dbfebea39181d58f95629a6a351314c94b7533eb737e7e69666482809e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "landing/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("landing/base.html.twig", "landing/pages/cookie.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Временные файлы cookie";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"content--page\">
        <div id=\"allrecords\" class=\"t-records\" style=\"overflow: hidden;\">
            <style>
                main {
                    font-size: 16px;
                }

                article {
                    padding: 16px 0
                }

                h3 {
                    margin: 0 0 12px;
                    font-size: 22px;
                }

                p {
                    margin: 0 0 12px 0;
                }
            </style>

            <main class=\"container\">
                <h1 class=\"h1\">Использование файлов cookie компанией EFSOL</h1>

                <article>
                    <p>На веб-сайтах и в онлайн-сервисах компании EFSOL могут использоваться файлы «cookie». Файлы
                        «cookie»
                        позволяют персонализировать просмотр наших сайтов. Они помогают нам отслеживать наиболее
                        посещаемые
                        веб-страницы, определять эффективность рекламы и интернет-поисков, а также дают представление о
                        поведении пользователей, что позволяет улучшать наши средства коммуникации и продукты.</p>

                    <p>Чтобы отключить файлы cookie в веб-браузере Google Chrome, откройте настройки Google Chrome,
                        кликните по
                        ссылке «Показать дополнительные настройки» и нажмите «Настройки контента». Установите флажок
                        «Удалять
                        локальные данные при закрытии браузера». Если вы используете другой веб-браузер, свяжитесь со
                        своим
                        провайдером, чтобы узнать, как отключить файлы cookie. </p>

                    <p>На наших веб-сайтах используются файлы «cookie». Отключив их, вы можете потерять доступ к
                        некоторым
                        разделам сайтов.</p>

                    <p>Файлы «cookie», используемые на наших веб-сайтах, подразделяются на категории в соответствии с
                        рекомендациями, приведенными в руководстве о файлах «cookie» Международной торговой палаты
                        Великобритании. Мы используем следующие категории на наших веб-сайтах и прочих
                        онлайн-сервисах:</p>
                </article>
                <article>
                    <h3>Категория 1 — строго необходимые файлы «cookie».</h3>
                    <p>Эти файлы «cookie» необходимы для просмотра наших веб-сайтов и использования их функций. Без них
                        невозможно обеспечить правильную обработку ваших заявок на обратную связь.</p>
                </article>
                <article>
                    <h3>Категория 2 — эксплуатационные файлы «cookie».</h3>
                    <p>Эти файлы «cookie» собирают информацию об использовании веб-сайтов, например о наиболее часто
                        посещаемых
                        страницах. Такие данные могут быть использованы для оптимизации наших веб-сайтов и упрощения
                        навигации.
                        Эти файлы «cookie» также используются нашими аффилированными лицами для того, чтобы определить,
                        перешли
                        ли вы на наш веб-сайт с сайта аффилированных лиц, воспользовались ли вы нашими сервисами и
                        купили ли вы
                        наши продукты в результате посещения веб-сайта, в том числе — какие именно продукты и сервисы вы
                        купили.
                        Эти файлы «cookie» не используются для сбора личной информации. Вся информация, собранная с их
                        помощью,
                        предназначена для статистических целей и остается анонимной.</p>
                </article>
                <article>
                    <h3>Категория 3 — функциональные файлы «cookie».</h3>
                    <p>Эти файлы «cookie» позволяют нашим веб-сайтам запомнить сделанный вами выбор при просмотре сайта.
                        Например, файл «cookie» может запомнить ваше местонахождение, что позволит нам отобразить наш
                        веб-сайт
                        на языке вашей страны. Эти файлы также могут использоваться для запоминания таких настроек, как
                        размер и
                        шрифт текста, а также других настраиваемых параметров сайта. Эти файлы также могут
                        использоваться для
                        отслеживания рекомендуемых продуктов во избежание повторения. Информация, предоставляемая такими
                        файлами
                        «cookie», не позволяет вас идентифицировать. Они не предназначены для отслеживания вашей работы
                        на
                        веб-сайтах, не имеющих отношение к EFSOL.</p>
                </article>
            </main>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "landing/pages/cookie.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/pages/cookie.html.twig", "/symfony/templates/landing/pages/cookie.html.twig");
    }
}
