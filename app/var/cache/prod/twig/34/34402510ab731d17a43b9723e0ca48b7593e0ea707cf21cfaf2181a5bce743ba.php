<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* landing/blocks/functional.html.twig */
class __TwigTemplate_5088b0cdef99ac1565994c175743adb2eee00ab95cb5c62937f70c9053bde163 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"r t-rec\" data-animationappear=\"off\" data-record-type=\"720\">
    <div class=\"t-cover\" bgimgfield=\"img\"
         style=\"height:600px; background-image:url(";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/background/1_20x.jpg"), "html", null, true);
        echo ");\">
        <div class=\"t-cover__carrier loaded\"
             data-content-cover-bg=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/background/1.jpg"), "html", null, true);
        echo "\"
             data-content-cover-height=\"600px\" data-content-cover-parallax=\"\"
             style=\"height: 600px; background-attachment: scroll; background-image: url(";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/background/1.jpg"), "html", null, true);
        echo ")\"></div>
        <div class=\"t-cover__filter\"
             style=\"height:600px;background-image: -moz-linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.70));background-image: -webkit-linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.70));background-image: -o-linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.70));background-image: -ms-linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.70));background-image: linear-gradient(top, rgba(15,15,15,0.90), rgba(15,15,15,0.70));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#190f0f0f', endColorstr='#4c0f0f0f');\"></div>
        <div class=\"t720\">
            <div class=\"t-container\">
                <div class=\"t-width t-width_10 t720__mainblock t-align_center\">
                    <div class=\"t-cover__wrapper t-valign_middle\" style=\"height:600px; position: relative;z-index: 1;\">
                        <div class=\"t720__mainwrapper\" data-hook-content=\"covercontent\">
                            <div class=\"t720__textwrapper\">
                                <div class=\"t720__title t-title t-title_sm t-margin_auto\"
                                     style=\"color:#f3f3f9;font-size:40px;font-weight:700;\" field=\"title\"> Проверьте Ваш
                                    сайт на уязвимости
                                </div>
                                <div class=\"t720__descr t-descr t-descr_xl t-margin_auto\"
                                     style=\"color:#f3f3f9;font-size:20px;font-weight:500;max-width:800px;\"
                                     field=\"descr\"><br><br>Введите сайт для проверки<br></div>
                            </div>
                            <div>
                                <div id=\"scanner_form\" class=\"t-form t-form_inputs-total_1 \">
                                    <div class=\"t-form__inputsbox\">
                                        <div class=\"t-input-group t-input-group_em\">
                                            <div class=\"t-input-block\"><input type=\"text\" autocomplete=\"url\"
                                                                              name=\"URL\"
                                                                              class=\"t-input\" value=\"\"
                                                                              placeholder=\"https://example.com\"
                                                                              id=\"scan_url\"
                                                                              style=\"color:#000000; background-color:#ffffff; \">
                                            </div>
                                        </div>
                                        <div class=\"t-form__submit\">
                                            <button type=\"submit\" class=\"t-submit\" id=\"scan_submit\"
                                                    style=\"color: rgb(15, 15, 15); background-color: rgb(255, 210, 0); border-radius: 0px; background-image: -webkit-gradient(radial, 154.391 76.0312, 674.595, 154.391 76.0312, 674.595, from(rgba(243, 243, 249, 0)), to(rgba(255, 255, 255, 0)));\">
                                                Сканировать
                                            </button>
                                        </div>
                                    </div>
                                    <div class=\"t-form__errorbox-bottom\" style=\"display: none;\">
                                        <div class=\"js-errorbox-all t-form__errorbox-wrapper\"
                                             style=\"display: block; height: 70px;\">
                                            <div class=\"t-form__errorbox-text t-text t-text_md\">
                                                <p class=\"t-form__errorbox-item js-rule-error\"
                                                   style=\"display: block;\">Укажите, пожалуйста, корректный адрес
                                                    сайта</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id=\"loader\" class=\"lds-dual-ring hidden overlay\"></div>
                                </div>
                                <style>
                                    input::-webkit-input-placeholder {
                                        color: #000000;
                                        opacity: 0.5;
                                    }

                                    input::-moz-placeholder {
                                        color: #000000;
                                        opacity: 0.5;
                                    }

                                    input:-moz-placeholder {
                                        color: #000000;
                                        opacity: 0.5;
                                    }

                                    input:-ms-input-placeholder {
                                        color: #000000;
                                        opacity: 0.5;
                                    }

                                    textarea::-webkit-input-placeholder {
                                        color: #000000;
                                        opacity: 0.5;
                                    }

                                    textarea::-moz-placeholder {
                                        color: #000000;
                                        opacity: 0.5;
                                    }

                                    textarea:-moz-placeholder {
                                        color: #000000;
                                        opacity: 0.5;
                                    }

                                    textarea:-ms-input-placeholder {
                                        color: #000000;
                                        opacity: 0.5;
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .t-submit:hover {
            background-color: #fdc700 !important;
            color: #0f0f0f !important;
        }

        .t-submit {
            -webkit-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
            transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
        }
    </style>
    <style>
        .t-submit[data-btneffects-first],
        .t-submit[data-btneffects-second] {
            position: relative;
            overflow: hidden;
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }
    </style>
</div>";
    }

    public function getTemplateName()
    {
        return "landing/blocks/functional.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 7,  46 => 5,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "landing/blocks/functional.html.twig", "/symfony/templates/landing/blocks/functional.html.twig");
    }
}
