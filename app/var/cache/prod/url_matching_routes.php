<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/make_report' => [[['_route' => 'make_report', '_controller' => 'App\\Controller\\ReportController::scan'], null, ['POST' => 0], null, false, false, null]],
        '/pdf' => [[['_route' => 'make_pdf', '_controller' => 'App\\Controller\\ReportController::makePdf'], null, ['GET' => 0], null, false, false, null]],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\ScannerController::index'], null, ['GET' => 0], null, false, false, null]],
        '/report' => [[['_route' => 'report', '_controller' => 'App\\Controller\\ScannerController::report'], null, ['POST' => 0], null, false, false, null]],
        '/about' => [[['_route' => 'about', '_controller' => 'App\\Controller\\ScannerController::about'], null, ['GET' => 0], null, false, false, null]],
        '/contacts' => [[['_route' => 'contacts', '_controller' => 'App\\Controller\\ScannerController::contacts'], null, ['GET' => 0], null, false, false, null]],
        '/cookie' => [[['_route' => 'cookie', '_controller' => 'App\\Controller\\ScannerController::cookie'], null, ['GET' => 0], null, false, false, null]],
        '/privacy-policy' => [[['_route' => 'privacy-policy', '_controller' => 'App\\Controller\\ScannerController::privacyPolicy'], null, ['GET' => 0], null, false, false, null]],
        '/request_call' => [[['_route' => 'request_call', '_controller' => 'App\\Controller\\ScannerController::requestCall'], null, ['POST' => 0], null, false, false, null]],
    ],
    [ // $regexpList
    ],
    [ // $dynamicRoutes
    ],
    null, // $checkCondition
];
